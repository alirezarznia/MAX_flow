#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<pii,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
vector<pii> vec[1001];
map <pii ,ll> mp;
ll lev[1001] ,n , m ;
ll str[1001];
bool BFS(ll ss , ll tt){
    queue<ll> q;
    q.push(ss);
    Rep(i , n)  lev[i]=-1;
    lev[ss]=0;
    while(!q.empty()){
        ll cur = q.front(); q.pop();
        if(cur==tt) return true;
        Rep(i, vec[cur].size()){
            pii cc = vec[cur][i];
            if(lev[cc.first]==-1 && cc.second != 0){
                q.push(cc.first);lev[cc.first]=lev[cur]+1;
            }
        }
    }
    return false;
}
ll sfw(ll ss ,ll tt ,ll mf){
    if(ss== tt) return mf;
    for( ;str[ss] < vec[ss].size(); str[ss]++){
        pii cur = vec[ss][str[ss]]; ll v = cur.first ,  w=cur.second;
        if(lev[v] == lev[ss]+1 && w != 0){
            ll cmf = min(mf , w);
            ll as =sfw(v, tt ,cmf);
            if(as >0){
                vec[ss][str[ss]].second-=as;

                vec[v][mp[MP(v, ss)]].second+=as;
                return as;
            }
        }
    }
    return 0;
}
ll D(ll ss , ll tt){
    ll to =0;
    while(BFS(ss ,tt)){
        Set(str,0);
        while(ll flow = sfw(ss, tt , inf))    to+=flow;
    }
    return to;
}

int main(){
  //  Test;
    ll t;
    cin>>t;
    while(t--)
    {
        cin>>n>>m;
        Rep(i , m){ll x , y , z ; cin>>x>>y>>z ; x-- ; y--; if(mp.find(MP(x, y)) != mp.end()) {ll cur = mp[MP(x,y)] , curt=mp[MP(y,x)]; vec[x][cur].second+=z,vec[y][curt].second+=z;}
            else{
                vec[x].push_back(MP(y,z));
                vec[y].push_back(MP(x,z));
                mp[MP(x,y)]=vec[x].size()-1;
                mp[MP(y,x)]=vec[y].size()-1;
            }
        }
        ll ss = 0 ; ll tt = n-1;
        cout<<D(ss , tt)<<endl;
        mp.clear();
        Rep(i ,n )  vec[i].clear();
    }
}
